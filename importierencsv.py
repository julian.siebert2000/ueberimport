
import csv
import pandas as pd

# input2 csv
class InputCsv:

    # class csv iniz
    def __init__(self):
        self.__csv_daten = None
        self.__output = []
        return

    # funktion: datei öffnen und lesen
    def input(self):
        df = pd.read_csv('name.csv', index_col=0)
        for value in df.values:
            self.__output.append(value[0])
            self.__output.append(value[-1])
        return

    def output(self):
        return self.__output
