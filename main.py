from pyad import *
import os, sys

import addinsad
import importierencsv
import manuelleeingabe
import db

# Fragt was willst du? choice
print("Hallo und herzlich Willkomen zu unserem Programm um automatisiert User im AD anzulegen.")
print("Es gibt 3 Optionen die Sie wählen können")
print("Option 1: Manuelle Abfrage")
print("Option 2: CSV-Datein auslesen")
print("Option 3: Datenbank auslesen")
print("Option 4: Programm beenden")

while True:
    options = int(input("Welche Option möchten Sie wählen? Bitte hier die entsprechende Zahl eingeben:"))

    # Manuel
    if options == 1:
        manuell = manuelleeingabe.manuell()
        manuell.input()

        ad_importing = addinsad.add()
        ad_importing.acquire(manuell.output_anmeldename(), manuell.output_pass(), manuell.output_nachname(), manuell.output_vorname())
        ad_importing.add()

        print("Benutzter wurde ins AD hinzugefügt!")

    # CSV
    if options == 2:
        csv = importierencsv.InputCsv()
        csv.input()
        ad_importing = addinsad.add()
        i = 0
        while i < len(csv.output()):
            ad_importing.acquire(csv.output()[i], csv.output()[i+1])
            ad_importing.add()
            print(csv.output()[i], csv.output()[i+1])
            i = i + 2
            pass
        exit(0)

    # Datenbank
    if options == 3:
        datenbunk = db.datenbank_import()
        datenbunk.connect()
        datenbunk.select_database()
        datenbunk.close_connection()
        ad_importing = addinsad.add()
        for value in datenbunk.output():
            ad_importing.acquire(value[0], value[1])
            ad_importing.add()
        exit(0)


    # Exit
    if options == 4:
        exit(0)

    else:
        print("Bitte geben sie eine Zahl von 1 bis 4 an um eine der 4 Option zu wählen!")

exit(0)