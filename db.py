

import mysql.connector as mariadb

#connect

class datenbank_import:

    def __init__(self):
        self.__connection = None
        self.__cursor = None
        self.__boy = None
        return

    def connect(self):
        try:
            self.__connection = mariadb.connect(
                user='root',
                password='12345687!q',
                host='127.0.0.1',
                port='3306'
            )

        except mariadb.Error as e:
            exit(f"Error connecting to MariaDB: {e}")
        print("Connected with: ", '127.0.0.1')

    def select_database(self):
        self.__cursor = self.__connection.cursor()
        self.__cursor.execute("USE adtest;")
        self.__cursor.execute("SELECT username, passwort FROM users;")
        self.__boy = self.__cursor.fetchall()

    def close_connection(self):
        self.__cursor.close()
        self.__connection.close()

    def output(self):
        return self.__boy
